from matplotlib import pyplot as plt

temps=open("Tdie.txt","r").read()
temps=temps.split()

usage=open("CPUsage.txt","r").read()
usage=usage.split()

for i in range(len(temps)):
    temps[i]=float(temps[i])

for i in range(len(usage)):
    usage[i]=float(usage[i])


plt.plot(temps, label="temps")
plt.plot(usage, label="usage")
plt.legend()
plt.show()
