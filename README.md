# HOW TO USE
1. Run tempRecorder.sh
    - This will record the temperature  and CPU usage at 1 second intervals and put the values in their own files
2. running grapher3.py will give you a graph of CPU usage and temperature over the period tempRecorder.sh was run.

![graph](https://gitlab.com/RJHopper/temputility/-/raw/master/YYEEEE.png)