#!/bin/bash

COUNT=0

rm counter.txt Tdie.txt Tctl.txt CPUsage.txt

while [ True ]; do

sensors| awk '/^Tdie:/ {print $2}' | cut -c 2-5 >> Tdie.txt
sensors| awk '/^Tctl:/ {print $2}' | cut -c 2-5 >> Tctl.txt
neofetch cpu_usage | cut -c 12-13 >> CPUsage.txt
echo "$COUNT" >> counter.txt
COUNT=$[COUNT + 1]
sleep 1
done 
