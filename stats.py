import statistics as stats

temps=open("Tdie.txt","r").read()    
temps=temps.split()

usage=open("CPUsage.txt","r").read()
usage=usage.split()

for i in range(len(temps)):
    temps[i]=float(temps[i])

for i in range(len(usage)):
    usage[i]=float(usage[i])


print("Average temperature:\t ", int(stats.mean(temps)),"°C")
print("Average CPU usage:\t ", int(stats.mean(usage)),"%")

